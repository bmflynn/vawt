
#include <RFM69.h>
#include <SPI.h>
#include <SPIFlash.h>
#include <WirelessHEX69.h>

#define MY_ID 1 
#define TARGET_ID 2
#define NETID 1 
// key has to be 16 bytes
#define NETKEY "MySecretKeyFFFFF"
#define ACK_TIME 50 
#define TIMEOUT 3000
#define FREQUENCY RF69_433MHZ

#define STR_LEN 256
#define BAUD_RATE 115200 

#define LED_PIN 9

RFM69 radio;

void setup() {

  Serial.begin(BAUD_RATE);

  radio.initialize(FREQUENCY, MY_ID, NETID);
  radio.encrypt(NETKEY);
  Serial.println("Radio initialized");

  pinMode(LED_PIN, OUTPUT);
}


void handleSerialInput() {
	char input[STR_LEN];
	byte inputLen = readSerialLine(input);
	// serial input looks like firmware
	if (inputLen == 4 && input[0] == 'F' && input[1] == 'L' && input[2] == 'X' && input[3] == '?') {
		CheckForSerialHEX((byte*)input, inputLen, radio, TARGET_ID, TIMEOUT, ACK_TIME, true);

	// other serial input
	} else if (inputLen > 0) {
		Serial.print("Serial Input:[");
		Serial.print(input);
		Serial.println("]");
	}
}


void handleRadioInput() {
	if (radio.receiveDone()) {
		Serial.print("Radio Input:[");
		for (byte idx=0; idx<radio.DATALEN; idx++) {
			Serial.print((char)radio.DATA[idx]);
		}
		Serial.print("]");
		if (radio.ACK_REQUESTED) {
			radio.sendACK();
			Serial.print(" - ACK sent");
		}
		Serial.println();
	}

}


void loop() {

	handleSerialInput();
	handleRadioInput();

	digitalWrite(LED_PIN, HIGH);
	delay(10);
	digitalWrite(LED_PIN, LOW);
}