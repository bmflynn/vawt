
#ifndef DATA_H
#define DATA_H

#define FIELD_SEP ","

class Data {
public:
	static const int MISSING;
  float voltage;
  float current1;
  float current1_fault;
  float current2;
  float current2_fault;
  float current3;
  float current3_fault;
  float current4;
  float current4_fault;
  float temp1;
  float temp2;

	Data();
	void toString(char * line);
private:
	void appendFloatValue(float value, char * line, char * sep);
	void appendIntValue(int value, char * line, char * sep);
};

#endif
