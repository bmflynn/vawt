
#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <Adafruit_RGBLCDShield.h>

#include "Data.hpp"
#include "Display.hpp"

#define OFF 0x0
#define ON 0x7

Display::Display(unsigned long timeout) {
	timer = 0;
	this->timeout = timeout;
	// Uses I2C pins
	lcd = Adafruit_RGBLCDShield();
	displayOn = false;
	lastScreen = 1;
}

void Display::begin() {
	lcd.begin(16, 2);
	this->off();
}

void Display::on(unsigned long millis) {
	timer = millis;
	lcd.display();
	lcd.setBacklight(ON);
	displayOn = true;
}

void Display::off() {
	timer = 0;
	lcd.noDisplay();
	lcd.setBacklight(OFF);
	displayOn = false;
}

bool Display::isOn() {
	return displayOn;
}

bool Display::timerExpired(unsigned long millis) {
	return (millis - timer) >= timeout;
}

void Display::printScreen1(Data * sensors) {
	lastScreen = 1;
	char line[17];
	lcd.clear();
	lcd.setCursor(0, 0);
	             //xxxxxxxxxxxxxxxx
	sprintf(line, "Current DC: %d", (int)sensors->current4);
	lcd.print(line);

	lcd.setCursor(0, 1);
	             //xxxxxxxxxxxxxxxx
	sprintf(line, "AC: %d %d %d",
						(int)sensors->current1,
						(int)sensors->current2,
						(int)sensors->current3);
	lcd.print(line);
}

void Display::printScreen2(Data * sensors) {
	lastScreen = 2;
	char line[17];
	lcd.clear();
	lcd.setCursor(0, 0);
	             //xxxxxxxxxxxxxxxx
	sprintf(line, "Volts:%d", (int)sensors->voltage);
	lcd.print(line);
	lcd.setCursor(0, 1);
	             //xxxxxxxxxxxxxxxx
	sprintf(line, "Air:%d H2O:%d", 
						(int)sensors->temp1,
						(int)sensors->temp2);
	lcd.print(line);
}

void Display::print(Data * sensors) {
	if (lastScreen == 1) {
		printScreen2(sensors);
	} else {
		printScreen1(sensors);
	}
}

bool Display::selectPressed() {
	int buttons = lcd.readButtons();
	if (buttons & BUTTON_SELECT) {
		return true;
	}
	return false;
}


