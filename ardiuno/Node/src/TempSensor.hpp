
#ifndef TEMPSENSOR_H
#define TEMPSENSOR_H

#include <OneWire.h>
#include <DallasTemperature.h>

#define DALLAS_PREC 9

class TempSensor {

public:
	TempSensor(int pin);
	float getTemperature();
	bool init();

private:
	OneWire bus;
	DallasTemperature sensor;
	DeviceAddress addr;
  bool initialized = false;
};

#endif
