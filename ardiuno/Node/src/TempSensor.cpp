
#include <OneWire.h>
#include <DallasTemperature.h>

#define DALLAS_PREC 9

class TempSensor {

public:
	TempSensor(int pin);
	float getTemperature();
	bool init();

private:
	OneWire bus;
	DallasTemperature sensor;
	DeviceAddress addr;
  bool initialized = false;
};


TempSensor::TempSensor(int pin): bus(pin), sensor(&bus) {
}


float TempSensor::getTemperature() {
	return sensor.getTempF(addr); 
}


bool TempSensor::init() {
	sensor.begin();
	// get the first(only) address on the bus
	if (sensor.getAddress(addr, 0)) {
		initialized = true;
		sensor.setResolution(addr, DALLAS_PREC);
	} else {
		initialized = false;
	}
	return initialized;
}
