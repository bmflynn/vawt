
#include <Wire.h>
#include <Adafruit_MCP23017.h>
#include <Adafruit_RGBLCDShield.h>

#include <RFM69.h>
#include <SPI.h>
#include <SPIFlash.h>
#include <WirelessHEX69.h>

#include "TempSensor.hpp"
#include "Data.hpp"
#include "Display.hpp"

#define MY_ID 2 
#define GATEWAY_ID 1
#define NETID 1 
// key has to be 16 bytes
#define NETKEY "MySecretKeyFFFFF"
#define FREQUENCY RF69_433MHZ

#define STR_LEN 256
#define BAUD_RATE 115200 

// device loop interval
#define DEVICE_INTERVAL 100
#define DATA_INTERVAL 10000
#define DISPLAY_INTERVAL 3000
#define DISPLAY_TIMEOUT 15000

//
// Analog pins
#define CURRENT1_PIN A0
#define CURRENT2_PIN A1
#define CURRENT3_PIN A2
#define CURRENT4_PIN A3
// A4-5 are being used for I2C for the display
#define VOLTAGE_PIN A6 

//
// Digital pins
#define TEMP1_PIN 0
#define TEMP2_PIN 1 
#define CURRENT1_FAULT_PIN 2
#define CURRENT2_FAULT_PIN 3
#define CURRENT3_FAULT_PIN 4
#define CURRENT4_FAULT_PIN 5
#define FLASH_SS_PIN 8
#define LED_PIN 9
#define RADIO_SS_PIN 10
// D11-13 are being used for SPI for the flash/radio
// D18-19 are being used for I2C for the display

// reference voltate in mV used to calibrate current sensor
#define VREF 1100
// Moetino VCC
#define VCC 3.3
// Volts per step of ADC resolution
#define VSTEP VCC / 1024 
// current sensor sensitivity from dataseet in volts
#define CURRENT_SENSITIVITY 0.11

TempSensor temp1(TEMP1_PIN);
TempSensor temp2(TEMP2_PIN);

RFM69 radio(RADIO_SS_PIN);
SPIFlash flash(FLASH_SS_PIN, 0xEF30);

Display display(DISPLAY_TIMEOUT);
Data sensors;

/*
 * Read the internal AVcc in volts.
 *
 * See: https://code.google.com/p/tinkerit/wiki/SecretVoltmeter
 */
float readIntVcc() {
  long result;
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC);
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = (VREF * 1024L) / result;
  return result / 1000.0;
}


/*
 * Read the current pin and scale it based on the internal reference voltate.
 */
float readCurrent(int pin) {
	float vccInt = readIntVcc();
  return ((analogRead(pin) * VSTEP * vccInt / VCC) - (.5 * vccInt)) / CURRENT_SENSITIVITY;
} 


// populate the data struct
void readSensors(char * outputLine) {
	sensors.voltage = analogRead(VOLTAGE_PIN);

	sensors.current1 = readCurrent(CURRENT1_PIN);
	sensors.current1_fault = digitalRead(CURRENT1_FAULT_PIN);

	sensors.current2 = readCurrent(CURRENT2_PIN);
	sensors.current2_fault = digitalRead(CURRENT2_FAULT_PIN);

	sensors.current3 = readCurrent(CURRENT3_PIN);
	sensors.current3_fault = digitalRead(CURRENT3_FAULT_PIN);

	sensors.current4 = readCurrent(CURRENT4_PIN);
	sensors.current4_fault = digitalRead(CURRENT4_FAULT_PIN);

	sensors.temp1 = analogRead(TEMP1_PIN);
	sensors.temp2 = analogRead(TEMP2_PIN);

	sensors.toString(outputLine);
}


void handleDisplay(unsigned long millis) {
	if (display.selectPressed()) {
		display.on(millis);
	}
	if (display.isOn()) {
		if (display.timerExpired(millis)) {
			display.off();
		} 
		if (millis % DISPLAY_INTERVAL < DEVICE_INTERVAL) {
			display.print(&sensors);
		}
	}
}


void setup() {

  Serial.begin(BAUD_RATE);
	Serial.println("Serial initialized");

  radio.initialize(FREQUENCY, MY_ID, NETID);
  radio.encrypt(NETKEY);
  Serial.println("Radio initialized");

	display.begin();
	Serial.println("Display initialized");

  if (flash.initialize()) {
    Serial.println("SPIFlash initialized");
  } else {
    Serial.println("SPIFlash init failed!!");
  }

  pinMode(VOLTAGE_PIN, INPUT);
  pinMode(CURRENT1_PIN, INPUT);
  pinMode(CURRENT2_PIN, INPUT);
  pinMode(CURRENT3_PIN, INPUT);
  pinMode(CURRENT4_PIN, INPUT);
  pinMode(TEMP1_PIN, INPUT);
  pinMode(TEMP2_PIN, INPUT);
	pinMode(CURRENT1_FAULT_PIN, INPUT);
	pinMode(CURRENT2_FAULT_PIN, INPUT);
	pinMode(CURRENT3_FAULT_PIN, INPUT);
	pinMode(CURRENT4_FAULT_PIN, INPUT);
	pinMode(LED_PIN, OUTPUT);

}


void loop(void) {

	// all timing based in this time, i.e., this should be the
	// only call to millis()
	unsigned long current_time = millis();

	// Support for receiving program over the radio
  if (radio.receiveDone()) {
		Serial.println("Checking for firmware");
		CheckForWirelessHEX(radio, flash, false);
  }

	// only handle sensors every ~DATA_INTERVAL 
	if (current_time % DATA_INTERVAL < DEVICE_INTERVAL) {
		digitalWrite(LED_PIN, HIGH);

		char dataLine[STR_LEN];
		readSensors(dataLine);

		Serial.print("radio:");
		if (radio.sendWithRetry(GATEWAY_ID, dataLine, strlen(dataLine))) {
			Serial.println("ok");
		} else {
			Serial.println("fail");
		}

		digitalWrite(LED_PIN, LOW);
	}

	handleDisplay(current_time);

	delay(DEVICE_INTERVAL);
}
