
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "Data.hpp"

#define STR_LEN 256

const int Data::MISSING = -9999;

Data::Data() {
	voltage = MISSING;
	current1 = MISSING;
	current1_fault = MISSING;
	current2 = MISSING;
	current2_fault = MISSING;
	current3 = MISSING;
	current3_fault = MISSING;
	current4 = MISSING;
	current4_fault = MISSING;
	temp1 = MISSING;
	temp2 = MISSING;
};

void Data::toString(char * line) {
  strcpy(line, "");
	appendFloatValue(voltage, line, FIELD_SEP);
	appendFloatValue(current1, line, FIELD_SEP);
	appendFloatValue(current1_fault, line, FIELD_SEP);
	appendFloatValue(current2, line, FIELD_SEP);
	appendFloatValue(current2_fault, line, FIELD_SEP);
	appendFloatValue(current3, line, FIELD_SEP);
	appendFloatValue(current3_fault, line, FIELD_SEP);
	appendFloatValue(current4, line, FIELD_SEP);
	appendFloatValue(current4_fault, line, FIELD_SEP);
	appendFloatValue(temp1, line, FIELD_SEP);
	appendFloatValue(temp2, line, "\r\n");
}


void Data::appendFloatValue(float value, char * line, char * sep) {
  char buff[10];
  dtostrf(value, 2, 1, buff);
  strncat(line, buff, 10);
  strncat(line, sep, STR_LEN);
}


void Data::appendIntValue(int value, char * line, char * sep) {
	snprintf(line, STR_LEN, "%s%d%s", line, value, sep);
}
