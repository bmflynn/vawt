
#ifndef DISPLAY_H
#define DISPLAY_H

#include "Data.hpp"

class Display {
public:
	Display(unsigned long timeout);
	void off();
	void on(unsigned long millis);
	bool isOn();
	void printScreen1(Data * sensors);
	void printScreen2(Data * sensors);
	void print(Data * sensors);
	bool selectPressed();
	bool timerExpired(unsigned long millis);
	void begin();

private:
	int delay;	
	Adafruit_RGBLCDShield lcd;
	unsigned long timeout;
	unsigned long timer;
	bool displayOn;
	int lastScreen;
};

#endif
