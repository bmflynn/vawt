
# Tool Chain 
The code in the arduino directory is setup to be built with the ino tools.
Quick notes on setting them up:

    apt-get install gcc-avr binutils-avr avr-libc

The insall the [ino](http://inotool.org/) tool:

    pip install ino

    cat <<EOF > ~/.inorc
    [build]
    board-model = uno

    [upload]
    board-model = uno
    ; Use for FTDI
    ; serial-port = /dev/ttyAMA0
    serial-port = /dev/ttyUSB0
    baud-rate = 115200

    [serial]
    ; Use for FTDI
    ; serial-port = /dev/ttyAMA0
    serial-port = /dev/ttyUSB0
    baud-rate = 115200
    EOF


# Dependencies
These will need to get these installed somewhere the ino build tool can find 
them. I prefer to install them in the ``lib`` directory, but they can also be
installed in ``/usr/share/arduino/libraries``.

* [OneWire][1]
* [Arduino-Temperature-Control-Library][2]

# Building

    ino build








[1](http://www.pjrc.com/teensy/td_libs_OneWire.html)
[2](https://github.com/milesburton/Arduino-Temperature-Control-Library)