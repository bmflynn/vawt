try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='VWAT',
    version='0.1',
    zip_safe=False,
    install_requires=[
        'flask',
        'gevent',
        'gevent-websocket',
        'pyzmq',
        'pyserial'
    ],
    packages=find_packages(),
    entry_points="""
        [console_scripts]
        vwat_server=vwat.web:server
        vwat_ingest=vwat.ingest:main
    """,
)
