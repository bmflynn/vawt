import logging
import json

import zmq

LOG = logging.getLogger(__name__)

class Publisher(object):

    def __init__(self, url):
        self.url = url
        context = zmq.Context()
        self.socket = context.socket(zmq.PUB)
        self.socket.bind(self.url)

    def publish(self, source):

        for message in source:
            data = json.dumps(message)
            LOG.debug("publishing %s", data)
            self.socket.send(data)
