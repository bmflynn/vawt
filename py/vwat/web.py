import logging
import os
import json
import time
import random
from datetime import datetime

from flask import Flask, render_template
from gevent import monkey
monkey.patch_all()

from .sub import Subscriber

ACK = '\x06' 

LOG = logging.getLogger(__name__)

app = Flask('vwat')
app.secret_key = os.urandom(24)
app.debug = True

def VwatApp(environ, start_response):
    path = environ["PATH_INFO"]
    if path == "/socket":
        handle_websocket(environ['wsgi.websocket'])
    else:
        return app(environ, start_response)

def handle_websocket(socket):
    subscriber = Subscriber('tcp://127.0.0.1:8055')
    for message in subscriber.read():
        data = socket.receive()
        if data != ACK:
            LOG.info("received %s, closing", data)
            break
        socket.send(json.dumps(message))
        time.sleep(2)

@app.route("/")
def index():
    return render_template('guages.html')

@app.route("/sevensegment")
def sevensegment():
    return render_template('sevensegment.html')
