
ACK = '\x06'

COLORS = ['#D7BA48', '#A29251', '#A68400', '#FFD840', '#FFE373'];
COLORS = ['#91B1C4', '#768893', '#2F617F', '#B6D1E1', '#C1D5E1'];
SENSOR_COLORS = ['#91B1C4', '#B6D1E1', '#C1D5E1', '#B6D1E1'];

function sensor_color(idx) {
	return SENSOR_COLORS[idx % SENSOR_COLORS.length];
}

require([
	"dojo/parser",
	"dijit/registry",
	"dojo/query",
	"dojo/_base/array",
	"my/ReverseLinearScaler",
	"dojox/socket",
	"dojo/dom-style",
	"dojox/dgauges/components/default/CircularLinearGauge",
	"dojox/dgauges/components/default/SemiCircularLinearGauge",
	"dojox/dgauges/components/grey/VerticalLinearGauge",
	"my/SevenSegment",
	"dojo/domReady!",
	],
	function (parser, registry, query, array, ReverseLinearScaler, Socket, style) {

		parser.parse();

		array.forEach(
			["watts", "windSpd"],
			function (item) {
				registry.byId(item)
					.set("interactionMode", "none")
					.set("indicatorColor", "#cc0033");
			}
		);

		query(".sensor").forEach(function (item, idx) {
			var color = sensor_color(idx);
			console.log("" + idx + ": " + color);
			style.set(item, "background-color", color);	
		});

		socket = Socket("/socket");
		socket.on("open", function (evt) {
			console.log("opened");
			socket.send(ACK);
		});
		socket.on("message", function(evt) {
			var data = JSON.parse(evt.data);
			array.forEach(
				["watts", "windSpd", "volts", "curVwat", "curDump"], 
				function (item) {
					var val = Math.round(data[item] * 100) / 100;
					registry.byId(item).set("value", val);
				}
			);
			query("#updated").forEach(function (node) {
				node.innerHTML = new Date(data['timestamp']).toString();
			});
			socket.send(ACK);
		});
		socket.on("error", function (evt) {
			console.log(evt);
		});
		require("dojo/_base/unload").addOnUnload(socket.close);
	}
);
