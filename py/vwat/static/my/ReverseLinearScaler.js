
define(["dojox/dgauges/LinearScaler", "dojo/_base/declare"], function (LinearScaler, declare) {
	return declare("my.ReverseLinearScaler", LinearScaler, {
		positionForValue: function (value) {
			var position = this.inherited(arguments);
			return (1 - position);
		},
		valueForPosition: function (position) {	
			var value = this.inherited(arguments);
			return (this.maximum - value) + this.minimum;
		}
	});
});

