

define(["dojo/_base/declare",
		"dijit/_WidgetBase",
		"dijit/_TemplatedMixin",
		"dojo/text!./templates/SevenSegment.html"],
function (declare, _WidgetBase, _TemplatedMixin, template) {
	return declare([_WidgetBase, _TemplatedMixin], {
		templateString: template,
		baseClass:"sevenSegment",
		value: "---",
		title: "Unknown",
		_setValueAttr:{node:"valueNode", type:"innerHTML"},
		_setTitleAttr:{node:"titleNode", type:"innerHTML"}, 
		postCreate: function () {
			this.inherited(arguments);
		}
	});
});
