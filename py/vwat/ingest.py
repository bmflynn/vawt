import os
import logging
from logging.handlers import TimedRotatingFileHandler
import time
import random
from datetime import datetime

import serial

from . import pub

LOG = logging.getLogger(__name__)

def timestamp():
    return int(time.mktime(datetime.now().timetuple()) * 1000)

def between(mn, mx):
    return mn + random.random() * (mx - mn) 

def mock_read(**kwargs):
    num = 0
    while True:
        yield {
            'timestamp':timestamp(),
            'tempIn':between(90, 110),
            'tempOut':between(90, 110),
            'volts':between(22,28),
            'windSpd':between(0, 30),
            'curVwat':between(0,10),
            'curDump':between(0,10),
            'watts':between(0, 500),
            }
        num += 1
        time.sleep(2)

line_rexp = r'^(?:(-?\d*\.?\d+),?){7}$'
keys = ('tempIn', 'tempOut', 'volts', 'windSpd', 'curVwat', 'curDump', 'watts')
def serial_read(port, writter):
    assert port
    port = serial.Serial(port, 9600, 8, timeout=0.5)
    while True:
        line = port.readline(size=255)
        writter.write(line)
        if re.match(line_rexp, line):
            data = line.strip().split(',')
            message = {k:float(v) for k,v in zip(keys, data)}
            message['timestamp'] = timestamp()
            yield message
        else:
            LOG.debug("skipping: '%s'", line)

def configure_datalogger(datadir):
    """Use the logging system to handle the rotation of the logger files.
    
    NOTE: This means you *have* to have logging configured!
    """
    logger = logging.getLogger('datalogger')
    fpath = os.path.join(datadir, 'vwat.dat')
    handler = TimedRotatingFileHandler(fpath, when='D', utc=True)
    handler.setFormatter(logging.Formatter('%(message)s'))
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    logger.propagate = False

    return logger.info

def main():

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--pubport', type=int, default=8055)

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--serial', metavar='PORT')
    group.add_argument('--mock', action='store_true')

    parser.add_argument('datadir', default='')

    args = parser.parse_args()
    lvl = args.debug and logging.DEBUG or logging.INFO
    logging.basicConfig(level=lvl)

    if args.mock:
        reader = mock_read()
    else:
        writter = configure_datalogger(args.datadir)
        reader = serial_read(args.serial, writter)

    publisher = pub.Publisher('tcp://127.0.0.1:%d' % args.pubport) 
    LOG.info("publishing to %s", publisher.url)
    publisher.publish(reader)

