
import time
import random
from datetime import datetime

def between(mn, mx):
    return mn + random.random() * (mx - mn) 

class Instrument(object):

    def __init__(self, interval, count=None):
        self.interval = interval
        self.count = count

    def read(self):
        
        num = 0
        while True:
            yield {
                'timestamp':time.mktime(datetime.now().timetuple()) * 1000,
                'tempIn':between(90, 110),
                'tempOut':between(90, 110),
                'volts':between(22,28),
                'windSpd':between(0, 30),
                'curVwat':between(0,10),
                'curDump':between(0,10),
                'watts':between(0, 500),
                }
            num += 1
            if self.count and num > self.count:
                break 