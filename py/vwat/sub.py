import json
import time
import zmq

class Subscriber(object):

    def __init__(self, url):
        self.url = url
        context = zmq.Context()
        self.socket = context.socket(zmq.SUB)
        self.socket.setsockopt(zmq.SUBSCRIBE, '')
        self.socket.connect(self.url)

    def read(self):

        while True:
            data = self.socket.recv()
            if not data:
                break
            message = json.loads(data)
            yield message
