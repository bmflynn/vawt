<Qucs Schematic 0.0.15>
<Properties>
  <View=167,60,994,575,1.47724,0,0>
  <Grid=10,10,1>
  <DataSet=divider.dat>
  <DataDisplay=divider.dpl>
  <OpenDisplay=1>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND * 1 270 300 0 0 0 0>
  <Vdc V1 1 270 210 -63 -26 1 1 "5 V" 1>
  <.DC DC1 1 340 340 0 43 0 0 "26.85" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <.SW SW1 1 520 340 0 69 0 0 "DC1" 1 "lin" 1 "r" 1 "500" 1 "2000" 1 "79" 1>
  <R R1 1 360 150 -26 15 0 0 "r" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R3 1 550 150 -26 -59 1 0 "2.2 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R2 1 460 210 15 -26 0 1 "2.2k" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <270 240 270 280 "" 0 0 0 "">
  <270 280 270 300 "" 0 0 0 "">
  <270 280 460 280 "" 0 0 0 "">
  <460 150 520 150 "" 0 0 0 "">
  <580 150 630 150 "" 0 0 0 "">
  <630 150 630 280 "" 0 0 0 "">
  <460 280 630 280 "" 0 0 0 "">
  <460 150 460 180 "" 0 0 0 "">
  <460 240 460 280 "" 0 0 0 "">
  <390 150 460 150 "" 0 0 0 "">
  <270 150 270 180 "" 0 0 0 "">
  <270 150 330 150 "Vin" 250 100 19 "">
  <460 150 460 150 "Vout" 410 100 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
  <Text 690 100 18 #000000 0 "Voltage Divider\nR2 / (R1+R2)">
</Paintings>
