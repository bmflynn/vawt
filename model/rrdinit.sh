#!/usr/bin/env bash

# The natural frame rate of the data in seconds
INTERVAL=10
# Number of points in a year
NUM_PER_YEAR=$((86400*365/${INTERVAL}))

rrdtool create vawt.rrd \
    --step=10 \
    DS:wind_speed:GAUGE:10:U:U \
    DS:volts:GAUGE:10:U:U \
    DS:current1:GAUGE:10:U:U \
    DS:current2:GAUGE:10:U:U \
    DS:water_temp:GAUGE:10:U:U \
    DS:air_temp:GAUGE:10:U:U \
    RRA:AVERAGE:0.5:1:${NUM_PER_YEAR} \
    RRA:AVERAGE:0.5:6:$((${NUM_PER_YEAR}/6)) \
    RRA:AVERAGE:0.5:30:$((${NUM_PER_YEAR}/30)) \
    RRA:AVERAGE:0.5:336:$((${NUM_PER_YEAR}/336)) 